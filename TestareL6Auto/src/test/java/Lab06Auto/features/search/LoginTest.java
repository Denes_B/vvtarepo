package Lab06Auto.features.search;

import Lab06Auto.steps.serenity.*;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class LoginTest {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public LoginSteps loginSteps;

    @Steps
    public AccountSteps accountSteps;

    @Steps
    public CreateFolderSteps createFolderSteps;

    @Steps
    public CreateFolderOKSteps createFolderOKSteps;

    @Steps
    public LogoutSteps logoutSteps;

    @Test
    public void valid_login() {
        loginSteps.go_to_login_page();
        loginSteps.login_steps("linux.scs.ubbcluj.ro", "vvta", "vvta2018+");
        accountSteps.create_Folder();
        createFolderSteps.createFolder_steps("Folderbdci0058");
        createFolderOKSteps.back_to_page_login("Directory Folderbdci0058 was successfully created.");
        accountSteps.click_logout();
        logoutSteps.should_see_message("You have logged out from the FTP server.");
    }


} 