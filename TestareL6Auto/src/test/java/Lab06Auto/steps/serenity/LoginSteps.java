package Lab06Auto.steps.serenity;

import Lab06Auto.pages.LoginPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.StepGroup;


public class LoginSteps {

    LoginPage loginPage;

    @Step
    public void select_server(String serverName) {

        loginPage.select_server(serverName);
    }

    @Step
    public void enters_username(String user) {

        loginPage.enters_username(user);
    }

    @Step
    public void enters_password(String pass) {

        loginPage.enters_password(pass);
    }

    @Step
    public void click_login() {

        loginPage.click_login();
    }

    @Step
    public void go_to_login_page() {

        loginPage.open();
    }

    @StepGroup
    public void login_steps(String servername, String username, String password) {
        select_server(servername);
        enters_username(username);
        enters_password(password);
        click_login();
    }


}