package Lab06Auto.steps.serenity;

import Lab06Auto.pages.CreateFolderOKPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.StepGroup;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class CreateFolderOKSteps {

    CreateFolderOKPage createFolderOKPage;


    @Step
    public void should_see_message_directory(String message) {
        assertThat(createFolderOKPage.getDefinitions(), hasItem(containsString(message)));
    }
    @Step
    public void click_ButtonBack (){
        createFolderOKPage.click_ButtonBack();
    }

    @StepGroup
    public void back_to_page_login (String message){
        should_see_message_directory(message);
        click_ButtonBack();
    }

}
