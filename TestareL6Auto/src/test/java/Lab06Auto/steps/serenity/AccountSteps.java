package Lab06Auto.steps.serenity;

import Lab06Auto.pages.AccountPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class AccountSteps {

    AccountPage accountPage;

    @Step
    public void create_Folder (){
        accountPage.click_createFolder();
    }

    @Step
    public void click_logout() {

        accountPage.click_logout();
    }



    ////////////////////////////////
    @Step
    public void should_see_definition(String definition) {
        assertThat(accountPage.getDefinitions(), hasItem(containsString(definition)));
    }




}