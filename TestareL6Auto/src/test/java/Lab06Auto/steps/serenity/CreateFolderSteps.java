package Lab06Auto.steps.serenity;

import Lab06Auto.pages.CreateFolderPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.StepGroup;

public class CreateFolderSteps {

    CreateFolderPage createFolderPage;


    @Step
    public void enters_textField (String textfield){

        createFolderPage.enters_textField(textfield);
    }


    @Step
    public void create_Folder() {

        createFolderPage.click_createFolder();
    }



    @StepGroup
    public void createFolder_steps(String textfield) {
        enters_textField(textfield);
        create_Folder();
    }
}
