package Lab06Auto.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

public class CreateFolderOKPage extends PageObject {

    public List<String> getDefinitions() {
        WebElementFacade definitionList = find(By.tagName("div"));
        return definitionList.findElements(By.tagName("form")).stream()
                .map( element -> element.getText() )
                .collect(Collectors.toList());
    }
    @FindBy(xpath = "//*[@id=\"NewDirForm\"]/a/img")
    private WebElementFacade clickButtonback;

    public void click_ButtonBack (){
        clickButtonback.click();
    }
}
