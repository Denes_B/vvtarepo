package Lab06Auto.pages;

import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import net.serenitybdd.core.pages.WebElementFacade;
import java.util.stream.Collectors;

import net.serenitybdd.core.annotations.findby.FindBy;

import net.thucydides.core.pages.PageObject;

import java.util.List;

@DefaultUrl("https://www.cs.ubbcluj.ro/apps/ftp/")
public class LoginPage extends PageObject {

    @FindBy(name="ftpserver")
    private WebElementFacade ftpServer;

    @FindBy(name="username")
    private WebElementFacade user;

    @FindBy(name="password")
    private WebElementFacade pass;

    @FindBy(name="Login")
    private WebElementFacade loginButton;


    public void select_server(String serverName) {

        ftpServer.selectByValue(serverName);
    }

    public void enters_username(String username) {

        user.type(username);
    }

    public void enters_password(String password) {

        pass.type(password);
    }

    public void click_login() {

        loginButton.click();
    }



    ///////////////////////////////////////
    public List<String> getDefinitions() {
        WebElementFacade definitionList = find(By.tagName("ol"));
        return definitionList.findElements(By.tagName("li")).stream()
                .map( element -> element.getText() )
                .collect(Collectors.toList());
    }
}