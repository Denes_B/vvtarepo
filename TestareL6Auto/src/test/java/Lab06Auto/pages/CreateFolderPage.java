package Lab06Auto.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;


public class CreateFolderPage extends PageObject {


    @FindBy(xpath = "//*[@id=\"NewDirForm\"]/input[16]")
    private WebElementFacade createFieldFolder;


    @FindBy(xpath = "//*[@id=\"NewDirForm\"]/a[2]/img")
    private WebElementFacade clickButtoncreate;


    public void enters_textField (String textfield){
        createFieldFolder.type(textfield);
    }

    public void click_createFolder() {
        clickButtoncreate.click();
    }



}
