<!DOCTYPE html PUBLIC "XHTML 1.0 Transitional" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" dir="ltr"><head>
<meta http-equiv="Content-type" content="text/html;charset=iso-8859-1" />
<meta name="keywords" content="net2ftp, web, ftp, based, web-based, xftp, client, PHP, SSL, SSH, SSH2, password, server, free, gnu, gpl, gnu/gpl, net, net to ftp, netftp, connect, user, gui, interface, web2ftp, edit, editor, online, code, php, upload, download, copy, move, delete, zip, tar, unzip, untar, recursive, rename, chmod, syntax, highlighting, host, hosting, ISP, webserver, plan, bandwidth" />
<meta name="description" content="net2ftp is a web based FTP client. It is mainly aimed at managing websites using a browser. Edit code, upload/download files, copy/move/delete directories recursively, rename files and directories -- without installing any software." />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<link rel="shortcut icon" href="favicon.ico" />
<link rel="apple-touch-icon" href="favicon.png" />
<title>net2ftp - a web based FTP client</title>

			<!-- /skins/skins.inc.php -->
			<script type="text/javascript" src="skins/shinra/js/jquery-1.5.1.min.js"></script>
			<script type="text/javascript" src="skins/shinra/js/jquery-ui-1.8.13.custom.min.js"></script>
			<script type="text/javascript" src="skins/shinra/js/custom.js"></script>
			<script type="text/javascript" src="skins/shinra/js/superfish-1.4.8/js/hoverIntent.js"></script>
			<script type="text/javascript" src="skins/shinra/js/superfish-1.4.8/js/superfish.js"></script>
			<script type="text/javascript" src="skins/shinra/js/superfish-1.4.8/js/supersubs.js"></script>
			<script type="text/javascript" src="skins/shinra/js/poshytip-1.0/src/jquery.poshytip.min.js"></script>
		<link rel="stylesheet" type="text/css" href="https://www.cs.ubbcluj.ro/apps/ftp//skins/shinra/css/main.css.php?ltr=ltr&amp;image_url=https%3A%2F%2Fwww.cs.ubbcluj.ro%2Fapps%2Fftp%2F%2Fskins%2Fshinra%2Fimages" />

			<!-- /skins/skins.inc.php -->
			<link rel="stylesheet" href="skins/shinra/css/style.css.php?show_ads=no" type="text/css" media="screen" />
			<link rel="stylesheet" href="skins/shinra/skins/glossy/style.css" type="text/css" media="screen" />
			<link rel="stylesheet" href="skins/shinra/js/poshytip-1.0/src/tip-twitter/tip-twitter.css" type="text/css" />
			<link rel="stylesheet" href="skins/shinra/js/poshytip-1.0/src/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
		<style id="poshytip-css-tip-twitter" type="text/css">div.tip-twitter{visibility:hidden;position:absolute;top:0;left:0;}div.tip-twitter table, div.tip-twitter td{margin:0;font-family:inherit;font-size:inherit;font-weight:inherit;font-style:inherit;font-variant:inherit;}div.tip-twitter td.tip-bg-image span{display:block;font:1px/1px sans-serif;height:10px;width:10px;overflow:hidden;}div.tip-twitter td.tip-right{background-position:100% 0;}div.tip-twitter td.tip-bottom{background-position:100% 100%;}div.tip-twitter td.tip-left{background-position:0 100%;}div.tip-twitter div.tip-inner{background-position:-10px -10px;}div.tip-twitter div.tip-arrow{visibility:hidden;position:absolute;overflow:hidden;font:1px/1px sans-serif;}</style><style id="poshytip-css-tip-yellowsimple" type="text/css">div.tip-yellowsimple{visibility:hidden;position:absolute;top:0;left:0;}div.tip-yellowsimple table, div.tip-yellowsimple td{margin:0;font-family:inherit;font-size:inherit;font-weight:inherit;font-style:inherit;font-variant:inherit;}div.tip-yellowsimple td.tip-bg-image span{display:block;font:1px/1px sans-serif;height:10px;width:10px;overflow:hidden;}div.tip-yellowsimple td.tip-right{background-position:100% 0;}div.tip-yellowsimple td.tip-bottom{background-position:100% 100%;}div.tip-yellowsimple td.tip-left{background-position:0 100%;}div.tip-yellowsimple div.tip-inner{background-position:-10px -10px;}div.tip-yellowsimple div.tip-arrow{visibility:hidden;position:absolute;overflow:hidden;font:1px/1px sans-serif;}</style></head>
<body onload="">

<!-- Template /skins/shinra/header.template.php begin -->

	<!-- WRAPPER -->
	<div id="wrapper">
			
		<!-- HEADER -->
		<div id="header">

<!-- Template /skins/shinra/google_ad_top.template.php begin -->
<!-- Template /skins/shinra/google_ad_top.template.php end -->

			<img id="logo" src="skins/shinra/img/logo.png" alt="net2ftp" />

		</div>
		<!-- ENDS HEADER -->
			
		<!-- MAIN -->
		<div id="main">

			<!-- content -->
			<div id="content">
				
				<!-- title -->
				<div id="page-title">
					<span class="title">linux.scs.ubbcluj.ro</span>
					<div style="text-align: right; margin-top: 10px;">
						<form id="StatusbarForm" method="post" action="/apps/ftp/index.php">
<input type="hidden" name="ftpserver" value="linux.scs.ubbcluj.ro" />
<input type="hidden" name="ftpserverport" value="21" />
<input type="hidden" name="username" value="vvta" />
<input type="hidden" name="language" value="en" />
<input type="hidden" name="skin" value="shinra" />
<input type="hidden" name="ftpmode" value="automatic" />
<input type="hidden" name="passivemode" value="yes" />
<input type="hidden" name="protocol" value="FTP" />
<input type="hidden" name="viewmode" value="list" />
<input type="hidden" name="sort" value="" />
<input type="hidden" name="sortorder" value="" />
						<input type="hidden" name="state" value="browse" />
						<input type="hidden" name="state2" value="main" />
						<input type="hidden" name="directory" value="/home/scs/others/vvta" />
						<input type="hidden" name="url_withpw" value="/apps/ftp/index.php?ftpserver=linux.scs.ubbcluj.ro&amp;amp;ftpserverport=21&amp;amp;username=vvta&amp;amp;password_encrypted=6E45AF56D9627C8F1D&amp;amp;language=en&amp;amp;skin=shinra&amp;amp;ftpmode=automatic&amp;amp;passivemode=yes&amp;amp;protocol=FTP&amp;amp;viewmode=list&amp;amp;sort=&amp;amp;sortorder=&amp;amp;state=newdir&amp;amp;state2=&amp;amp;directory=%2Fhome%2Fscs%2Fothers%2Fvvta&amp;amp;entry=" />
						<input type="hidden" name="url_withoutpw" value="/apps/ftp/index.php?ftpserver=linux.scs.ubbcluj.ro&amp;amp;ftpserverport=21&amp;amp;username=vvta&amp;amp;language=en&amp;amp;skin=shinra&amp;amp;ftpmode=automatic&amp;amp;passivemode=yes&amp;amp;protocol=FTP&amp;amp;viewmode=list&amp;amp;sort=&amp;amp;sortorder=&amp;amp;state=login_small&amp;amp;state2=bookmark&amp;amp;go_to_state=newdir&amp;amp;go_to_state2=&amp;amp;directory=%2Fhome%2Fscs%2Fothers%2Fvvta&amp;amp;entry=" />
						<input type="hidden" name="text" value="net2ftp linux.scs.ubbcluj.ro" />
<a href="javascript:document.forms['StatusbarForm'].state.value='bookmark';document.forms['StatusbarForm'].submit();" title="Bookmark (accesskey h)" accesskey="h"><img src="https://www.cs.ubbcluj.ro/apps/ftp//skins/shinra/images/actions/bookmark.png" alt="Bookmark (accesskey h)" onmouseover="this.style.margin='0px';this.style.width='34px';this.style.height='34px';" onmouseout="this.style.margin='1px';this.style.width='32px';this.style.height='32px';" style="border: 0px; margin: 1px; width: 32px; height: 32px; vertical-align: middle;" /></a>
<a href="javascript:void(window.open('https://www.cs.ubbcluj.ro/apps/ftp//modules/help/help-user.html','Help','location,menubar,resizable,scrollbars,status,toolbar'));" title="Help (accesskey i)" accesskey="i"><img src="https://www.cs.ubbcluj.ro/apps/ftp//skins/shinra/images/actions/info.png" alt="Help (accesskey i)" onmouseover="this.style.margin='0px';this.style.width='34px';this.style.height='34px';" onmouseout="this.style.margin='1px';this.style.width='32px';this.style.height='32px';" style="border: 0px; margin: 1px; width: 32px; height: 32px; vertical-align: middle;" /></a>
<a href="javascript:document.forms['StatusbarForm'].state.value='logout';document.forms['StatusbarForm'].submit();" title="Logout (accesskey l)" accesskey="l"><img src="https://www.cs.ubbcluj.ro/apps/ftp//skins/shinra/images/actions/exit.png" alt="Logout (accesskey l)" onmouseover="this.style.margin='0px';this.style.width='34px';this.style.height='34px';" onmouseout="this.style.margin='1px';this.style.width='32px';this.style.height='32px';" style="border: 0px; margin: 1px; width: 32px; height: 32px; vertical-align: middle;" /></a>
						</form>
					</div>
				</div>
				<!-- ENDS title -->

<!-- Template /skins/shinra/header.template.php end -->
<!-- Template /skins/shinra/manage.template.php begin -->

				<div style="margin-left: 50px; margin-right: 20px;">

						<h1>
<img src="https://www.cs.ubbcluj.ro/apps/ftp//skins/shinra/images/titles/folder_new.png" alt="icon" style="width: 48px; height: 48px; vertical-align: middle;" />
Create new directories						</h1>

		<form name="NewDirForm" id="NewDirForm" action="/apps/ftp/index.php" method="post">
<input type="hidden" name="ftpserver" value="linux.scs.ubbcluj.ro" />
<input type="hidden" name="ftpserverport" value="21" />
<input type="hidden" name="username" value="vvta" />
<input type="hidden" name="language" value="en" />
<input type="hidden" name="skin" value="shinra" />
<input type="hidden" name="ftpmode" value="automatic" />
<input type="hidden" name="passivemode" value="yes" />
<input type="hidden" name="protocol" value="FTP" />
<input type="hidden" name="viewmode" value="list" />
<input type="hidden" name="sort" value="" />
<input type="hidden" name="sortorder" value="" />
			<input type="hidden" name="state" value="newdir" />
			<input type="hidden" name="state2" value="" />
				<input type="hidden" name="directory" value="/home/scs/others/vvta" />
			<input type="hidden" name="screen" value="2" />

<a href="javascript:document.forms['NewDirForm'].state.value='browse';document.forms['NewDirForm'].state2.value='main';document.forms['NewDirForm'].submit();" title="Back (accesskey b)" accesskey="b"><img src="https://www.cs.ubbcluj.ro/apps/ftp//skins/shinra/images/actions/back.png" alt="Back (accesskey b)" onmouseover="this.style.margin='0px';this.style.width='34px';this.style.height='34px';" onmouseout="this.style.margin='1px';this.style.width='32px';this.style.height='32px';" style="border: 0px; margin: 1px; width: 32px; height: 32px; vertical-align: middle;" /></a>
   

<a href="javascript:document.forms['NewDirForm'].submit();" title="Submit (accesskey v)" accesskey="v"><img src="https://www.cs.ubbcluj.ro/apps/ftp//skins/shinra/images/actions/button_ok.png" alt="Submit (accesskey v)" onmouseover="this.style.margin='0px';this.style.width='34px';this.style.height='34px';" onmouseout="this.style.margin='1px';this.style.width='32px';this.style.height='32px';" style="border: 0px; margin: 1px; width: 32px; height: 32px; vertical-align: middle;" /></a>

			<br /><br />
<!-- Template /skins/shinra/newdir1.template.php begin -->
The new directories will be created in <b>/home/scs/others/vvta</b>.<br /><br />
New directory name: <input type="text" class="input" name="newNames[1]" /><br /><br />
New directory name: <input type="text" class="input" name="newNames[2]" /><br /><br />
New directory name: <input type="text" class="input" name="newNames[3]" /><br /><br />
New directory name: <input type="text" class="input" name="newNames[4]" /><br /><br />
New directory name: <input type="text" class="input" name="newNames[5]" /><br /><br />
<!-- Template /skins/shinra/newdir1.template.php end -->
		</form>

				</div>

<!-- Template /skins/shinra/footer.php begin -->
			
			</div>
			<!-- ENDS content -->

		</div>
		<!-- ENDS MAIN -->

		<!-- FOOTER -->
		<div id="footer">


			<!-- Bottom -->
			<div id="bottom">
				Powered by <a href="http://www.net2ftp.com">net2ftp</a> on a template designed by <a href="http://www.luiszuno.com">Luiszuno</a>
				
				<div id="to-top" class="poshytip" title="To top"></div>
					
			</div>
			<!-- ENDS Bottom -->

		</div>
		<!-- ENDS FOOTER -->
		
	</div>
	<!-- ENDS WRAPPER -->

<!-- Template /skins/shinra/footer.php end --><!-- Template /skins/shinra/manage.template.php end -->



</body></html>